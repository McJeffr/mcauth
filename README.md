# McAuth service

This repository contains a small authentication service. This service makes use of JWT, MongoDB and bcrypt. When a user logs in via the login form that is also present in this repository, their credentials will be hashed and salted by bcrypt and verified against a MongoDB database containing all user credentials. If the credentials that were entered are correct, the service will create a JWT token that can be used by the user to authorize themselves at other places, using this service. A token can be verified by one of the endpoints of the API of this service.

There is a default config file, `config/default.js`, present. This config file can be copied to a `config/local.js` to set up a local test environment, or a `config/production.js` for a production environment. The config file contains the JWT secret, which is the password used by JWT to encrypt and decrypt tokens. Next to that, the config file also contains information about the MongoDB connection, following the structure of [mongodb-uri](https://www.npmjs.com/package/mongodb-uri).

## Interface

POST `/api/authenticate`: Authenticates a user with its credentials, body is www-url-form-encoded and contains the username and password fields that are filled in.
GET `/api/verify`: Verifies a token that is placed in the header, named `x-access-token`.
GET `/?referrer=http://google.com/`: Shows the UI view where the user can authenticate. After this has been done, the user is redirected back to whatever is in the referrer URL parameter, google in this case.

## Disclaimer

This repository is a small project created for educational purposes. It is not meant to be used in any production environment, nor is it finished. It still has its insecurities and is not very generic yet to the point where it can be easily used standalone. It is merely used for experimenting with authentication systems running in NodeJS.