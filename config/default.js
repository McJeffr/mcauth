module.exports = {
	port: 8080,
	jwtSecret: 'SECRET',
	cookieSecret: 'SECRET',
	tokenLifetime: 86400, // 24 hours
	database: {
		hosts: [
			{
				host: 'localhost',
				port: 27017
			}
		],
		database: 'mcauth'
	}
};