// =================================================================
// Import all the required modules =================================
// =================================================================
var bcrypt       = require('bcrypt-nodejs');
var bodyParser   = require('body-parser');
var config       = require('config');
var cookieParser = require('cookie-parser');
var express      = require('express');
var jwt          = require('jsonwebtoken');
var mongoose     = require('mongoose');
var mongouri     = require('mongodb-uri');

var api             = require('./api');
var exceptionModule = require('./module/exception');
var logger          = require('./module/logger');
var User            = require('./model/user.js');

// =================================================================
// Configuration ===================================================
// =================================================================
var port = process.env.PORT || config.port;

// Create a database connection
mongoose.connect(mongouri.formatMongoose(config.database));

// Fetch the database connection object
var db = mongoose.connection;
db.on('error', function(err) {
	if (err) logger.error('Could not establish connection to the database:', err.toString());
});

// Construct the express app
var app = express();

// Apply the body-parser middleware to read request bodies
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser(config.cookieSecret));

// =================================================================
// APIs ============================================================
// =================================================================

// Apply API v1 to /api/v1
app.use('/api/v1', api.v1);

// =================================================================
// UI View =========================================================
// =================================================================

// Serve static content from the 'static' folder.
app.use('/', express.static('src/static'));

// =================================================================
// Start the service ===============================================
// =================================================================
app.listen(port);
logger.info('McAuth service is now running at port', port);
