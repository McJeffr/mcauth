// =================================================================
// Import all the required modules =================================
// =================================================================
var bcrypt = require('bcrypt-nodejs');
var config = require('config');
var jwt    = require('jsonwebtoken');

var logger = require('../../../module/logger');
var User   = require('../../../model/user.js');

// =================================================================
// API Route =======================================================
// =================================================================
module.exports = function(req, res) {

	// Read the data from the request body
	var uid = req.body.uid
	var password = req.body.password
	if (!uid || !password) {
		res.exception(4000);
		logger.info('%s tried to authenticate with an invalid request body', req.ip);
		return;
	}

	// Find the user with the provided uid in the database
	User.find({ uid }, function (err, result) {
		if (err) {
			logger.error('An exception occurred whilst trying to find a user:', err.toString());
			res.exception(5001);
			return;
		}

		// Check if there is a user with the provided uid
		if (result.length == 0) {
			res.exception(4001);
			return;
		}

		// Extract the user from the array, which only contains 1 user
		var user = result[0];

		// Verify that the password is correct
		if (!bcrypt.compareSync(password, user.password)) {
			logger.info('uid %s from ip %s tried to authenticate with an invalid password', uid, req.ip);
			res.exception(4001);
			return;
		}

		// Create a new token for this user
		var payload = { uid };
		var token = jwt.sign(payload, config.jwtSecret, {
			expiresIn: config.tokenLifetime
		});

		// Set a cookie with the token in it
		res.cookie('x-access-token', token, { maxAge: config.tokenLifetime * 1000, httpOnly: true, signed: true });

		// Return the token back to the user
		res.json({
			success: true,
			message: 'Token created.'
		});

		// Log the token creation
		logger.info('Created token for uid %s from ip %s', uid, req.ip);
	});

};