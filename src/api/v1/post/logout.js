// =================================================================
// Import all the required modules =================================
// =================================================================
var logger = require('../../../module/logger');

// =================================================================
// API Route =======================================================
// =================================================================
module.exports = function(req, res) {

	// Delete the token cookie
	res.clearCookie('x-access-token');

	// Log the successful logout
	logger.info('%s logged out', req.ip);

	// Return the status message of the logout
	res.json({
		success: true,
		message: 'Logged out successfully.'
	});

};