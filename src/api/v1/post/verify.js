// =================================================================
// Import all the required modules =================================
// =================================================================
var config = require('config');
var jwt    = require('jsonwebtoken');

var logger = require('../../../module/logger');

// =================================================================
// API Route =======================================================
// =================================================================
module.exports = function(req, res) {

	// Get the token from the request body
	var token = req.body.token;
	if (!token) {
		res.exception(4004);
		logger.info('%s tried to verify a token, but no token was provided in the request body', req.ip);
		return;
	}

	// Decode the token
	jwt.verify(token, config.jwtSecret, function(err, decoded) {
		// Verify the token to make sure it is not fake			
		if (err) {
			logger.info('%s tried to verify an invalid token', req.ip);
			res.json({
				success: false,
				message: 'Token is invalid.'
			});
			return;
		}

		// Log the token verification
		logger.info('%s has verified a token successfully', req.ip);

		// Return the success in the response object
		res.json({
			success: true,
			message: 'Token is valid'
		});
	});

};