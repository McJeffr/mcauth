// =================================================================
// Import all the required modules =================================
// =================================================================
var config = require('config');
var jwt    = require('jsonwebtoken');

var logger = require('../../../module/logger');

// =================================================================
// API middleware ==================================================
// =================================================================
module.exports = function(req, res, next) {

	// Get the token from the cookie
	var token = req.signedCookies['x-access-token'];
	if (!token) {
		res.exception(4002);
		logger.info('%s tried to authenticate, but no token was present', req.ip);
		return;
	}

	// Decode the token
	jwt.verify(token, config.jwtSecret, function(err, decoded) {
		// Verify the token to make sure it is not fake
		if (err) {
			logger.info('%s tried to authenticate, possibly with an invalid token', req.ip);
			res.exception(4003);
			return;
		}

		// Log the successful token verification
		logger.info('Token verified for uid %s from ip %s', decoded.uid, req.ip);

		// Add decoded token and continue to the next middleware
		req.tokenPayload = decoded;
		next();
	});

};