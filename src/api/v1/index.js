// =================================================================
// Import all the required modules =================================
// =================================================================
var express = require('express');

var exceptionModule = require('../../module/exception');

// =================================================================
// Construct the router of the API =================================
// =================================================================
var api = express.Router();

// Apply the exception middleware to the router
api.use(exceptionModule);

// =================================================================
// Add API routes ==================================================
// =================================================================
api.post('/verify', require('./post/verify.js'));
api.post('/login',  require('./post/login.js'));

api.use(require('./middleware/auth.js'));

api.post('/logout', require('./post/logout.js'));

// =================================================================
// Export API router ===============================================
// =================================================================
module.exports = api;