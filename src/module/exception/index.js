// =================================================================
// Import all the required modules =================================
// =================================================================
var exceptions = require('./exceptions.js');

// =================================================================
// Middleware ======================================================
// =================================================================
module.exports = function(req, res, next) {
	res.exception = function(code, optionals) {
		// Fetch the exception with the provided code, or the default error when an invalid code is provided
		var exception = exceptions[code] || exceptions[5000];

		// Set the HTTP status code and return the exception details
		res.status(exception.status);
		res.json(Object.assign(exception, optionals));
	}
	next();
};