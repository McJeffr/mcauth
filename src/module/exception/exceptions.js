// =================================================================
// Exception response objects ======================================
// =================================================================
module.exports = {
	4000: {
		id: 4000,
		status: 400,
		userMessage: "Invalid request body.",
		developerMessage: "The provided request body is not correct, please refer to the API documentation to see what the request body must contain"
	},
	4001: {
		id: 4001,
		status: 403,
		userMessage: "Invalid user credentials.",
		developerMessage: "The credentials provided in the request body could not be authenticated, they are incorrect."
	},
	4002: {
		id: 4002,
		status: 400,
		userMessage: "No access token present.",
		developerMessage: "An attempt to authenticate a token was made, but no cookie containing a token was present."
	},
	4003: {
		id: 4003,
		status: 403,
		userMessage: "Invalid access token.",
		developerMessage: "An attempt to authenticate a token was made, but the token could not be parsed or authenticated and is invalid."
	},
	4004: {
		id: 4004,
		status: 400,
		userMessage: "No access token present.",
		developerMessage: "An attempt the authenticate a token was made, but no token was present in the request body."
	},
	5000: {
		id: 5000,
		status: 500,
		userMessage: "Internal exception occurred.",
		developerMessage: "An exception with an invalid exception code was thrown in the API."
	},
	5001: {
		id: 5001,
		status: 500,
		userMessage: "Internal exception occurred.",
		developerMessage: "An exception related to the database connection of the service has occurred in the API."
	}
}