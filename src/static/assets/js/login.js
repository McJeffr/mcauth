(function($) {
	"use strict";
	
	// ---------------------------------------------------------
	// Function that gets fired when the form is submitted
	// ---------------------------------------------------------
	$("#login-form").submit(function() {
		$('#error').text('');
		submitForm($(this));
		return false;
	});

	// ---------------------------------------------------------
	// Function used to validate the input fields for input
	// ---------------------------------------------------------
	function validate(form) {
		var username = $('#username').val();
		var password = $('#password').val();

		if (!password || !username) {
			$('#error').text('One or more input fields are empty.');
			return false;
		} else {
			return true;
		}
	}

	// ---------------------------------------------------------
	// Function that processes the form submission
	// ---------------------------------------------------------
	function submitForm(form) {
		if (validate(form)) {
			// Get the redirect URL
			var referrer = getReferrerUrl();
			if (!referrer) {
				$('#error').text('Invalid request.');
				return;
			}

			// Fetch the username and password from the input fields
			var username = $('#username').val();
			var password = $('#password').val();

			// Send the data in a POST request to the API
			$.post("/api/v1/login", { uid: username, password }, function(data) {
				// If the authentication failed, let the user know
				if (!data.success) {
					$('#error').text(data.message);
					return;
				}

				// Redirect the user to where he came from
				window.location.replace(referrer);
			}).fail(function(err) {
				$('#error').text(err.responseJSON.userMessage);
			});
		}
	}

	// ---------------------------------------------------------
	// Function that is used to fetch the referrer URL
	// ---------------------------------------------------------
	function getReferrerUrl() {
		var url = decodeURIComponent(window.location.search.substring(1));
		var variables = url.split('&');

		for (var i = 0; i < variables.length; i++) {
			var parameter = variables[i].split('=');

			if (parameter[0] === 'referrer') {
				return parameter[1] === undefined ? true : parameter[1];
			}
		}
	};
		
})(jQuery);